export const environment = {
    env: 'prod',
    production: true,
    serverUrl: 'https://api.uniplusadmin.co.uk',
};
