export const environment = {
    production: false,
    env: 'dev',
    serverUrl: 'https://api.dev.uniplusadmin.co.uk',
};
