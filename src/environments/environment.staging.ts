export const environment = {
    env: 'staging',
    production: true,
    serverUrl: 'https://staging.dev.uniplusadmin.co.uk',
};
