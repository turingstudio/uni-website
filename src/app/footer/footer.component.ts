import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { transition, animate, style, state, trigger } from '@angular/animations';
import { UniversityService } from '../shared/services/university.service';
import { University } from '../shared/abstraction/university';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
    animations: [
        trigger('slideInOut', [
            state('out', style({
                overflow: 'hidden',
                height: '*',
            })),
            state('in', style({
                overflow: 'hidden',
                height: '101px',
            })),
            transition('in => out', animate('400ms ease-in-out')),
            transition('out => in', animate('400ms ease-in-out')),
        ]),
    ],
    encapsulation: ViewEncapsulation.None,
})
export class LandingFooterComponent implements OnInit {
    public universities: University[];
    public anim = 'in';
    constructor(private universityService: UniversityService) {}

    ngOnInit() {
        this.assureAndSubToUniversities();
    }

    assureAndSubToUniversities() {
        this.universityService.universities.subscribe(
            (resp: University[]) => this.universities = resp,
        );
    }

    expandAnim(): void {
        this.anim = (this.anim === 'in' ? 'out' : 'in');
    }
}
