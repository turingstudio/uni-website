import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/shared/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { Terms } from 'src/app/shared/abstraction/terms';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-terms',
    templateUrl: './terms.component.html',
    styleUrls: ['./terms.component.scss'],
})
export class TermsComponent implements OnInit {
    constructor(private dataService: DataService, public route: ActivatedRoute) {}
    subscriptions: Subscription[] = [];
    texts: string;
    isTerms = false;
    isPolicy = false;

    ngOnInit(): void {
        this.readTermsOrPolicy();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
    }

    readTermsOrPolicy(): void {
        this.subscriptions.push(this.route.url.subscribe(params => {
            if (params.toString() === 'terms-and-conditions') {
                this.dataService.getTerms().subscribe((terms: Terms) => (this.texts = terms.text));
                this.isTerms = true;
            } else {
                this.dataService.getPrivacyData().subscribe((terms: Terms) => (this.texts = terms.text));
                this.isPolicy = true;
            }
        }));
    }
}
