import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UniversityService } from './shared/services/university.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    constructor(private translate: TranslateService, private universityService: UniversityService) {
        translate.setDefaultLang('en');
        this.universityService.assure();
    }
    title = 'uniplus-website';
}
