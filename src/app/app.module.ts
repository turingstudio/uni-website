import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './shared/components/menu/menu.component';
import { HamburgerComponent } from './shared/components/hamburger/hamburger.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NgZorroAntdModule, NZ_I18N, en_US, NzFormModule, NzNotificationModule, NzInputModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import { InlineSVGModule } from 'ng-inline-svg';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import en from '@angular/common/locales/en';
import { LandingFooterComponent } from './footer/footer.component';
import { TermsComponent } from './terms/terms/terms.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { AboutUsHeaderComponent } from './about-us/about-us-header/about-us-header.component';
import { AboutUsDescriptionComponent } from './about-us/about-us-description/about-us-description.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LandingHeaderComponent } from './landing/landing-header/landing-header.component';
import { LandingPageComponent } from './landing/landing-page.component';
import { StatsComponent } from './shared/components/stats/stats.component';
import { LandingBenefitsComponent } from './landing/landing-benefits/landing-benefits.component';
import { InformationBoxComponent } from './landing/landing-benefits/information-box/information-box.component';
import { CallToActionComponent } from './shared/components/call-to-action/call-to-action.component';
import { UniversityComponent } from './university/university.component';
import { UniHeaderComponent } from './university/uni-header/uni-header.component';
import { StoresComponent } from './shared/components/stores/stores.component';
import { WhyUniComponent } from './university/why-uni/why-uni.component';
import { FeaturesComponent } from './university/features/features.component';

registerLocaleData(en);
@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        HamburgerComponent,
        LandingFooterComponent,
        TermsComponent,
        AboutUsComponent,
        AboutUsHeaderComponent,
        AboutUsDescriptionComponent,
        ContactUsComponent,
        LandingHeaderComponent,
        LandingPageComponent,
        StatsComponent,
        InformationBoxComponent,
        LandingBenefitsComponent,
        CallToActionComponent,
        UniversityComponent,
        UniHeaderComponent,
        StoresComponent,
        WhyUniComponent,
        FeaturesComponent,
    ],
    imports: [
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        NzNotificationModule,
        NzFormModule,
        ReactiveFormsModule,
        NzInputModule,
        NgZorroAntdModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
        }),
        InlineSVGModule.forRoot(),
        NzPopoverModule,
        NzCollapseModule,
        BrowserAnimationsModule,
    ],
    providers: [{ provide: NZ_I18N, useValue: en_US }],
    bootstrap: [AppComponent],
})
export class AppModule {}
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}
