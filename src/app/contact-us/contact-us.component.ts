import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalService } from 'src/app/shared/services/global.service';
import { NzNotificationService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { ContactService } from '../shared/services/contact.service';

@Component({
    selector: 'app-contact-us',
    templateUrl: './contact-us.component.html',
    styleUrls: ['./contact-us.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ContactUsComponent implements OnInit {
    contactUsForm: FormGroup;
    contactUsTranslates: any;
    constructor(
        private formBuilder: FormBuilder,
        private globalService: GlobalService,
        private notification: NzNotificationService,
        private translateService: TranslateService,
        private contactServce: ContactService,
    ) {}

    ngOnInit(): void {
        this.translateService.get('contactUs').subscribe(translates => (this.contactUsTranslates = translates));
        this.contactUsForm = this.formBuilder.group(
            {
                name: [null, [Validators.required]],
                email: [null, [Validators.email, Validators.required]],
                message: [null, [Validators.required]],
            },
        );
    }
    submitForm(): void {
        this.globalService.markFormAsDirty(this.contactUsForm);
        if (this.contactUsForm.valid) {
            this.contactServce.create(this.contactUsForm.value).subscribe(
                () => {
                    this.notification.success(
                        this.contactUsTranslates.form.successMessage.title,
                        this.contactUsTranslates.form.successMessage.text,
                    );
                    this.contactUsForm.reset();
                },
                () => {
                    this.notification.error('Something wrong happen', 'Operation failed. If this repeats, please contact our staff');
                },
            );
        }
    }
}
