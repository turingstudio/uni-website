import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'menu-hamburger',
    templateUrl: './hamburger.component.html',
    styleUrls: ['./hamburger.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class HamburgerComponent implements OnInit {
    constructor() {}

    ngOnInit(): void {}
}
