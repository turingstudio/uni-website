import { Component, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { UniversityService } from '../../services/university.service';
import { University } from '../../abstraction/university';
import { Router, NavigationEnd, Event } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class MenuComponent implements OnInit {
    private subscriptions: Subscription[] = [];
    public universities: University[];
    menuMobileExpandedStatus = false;
    menuMobileClosingUp = false;
    isScrolled: boolean = false;
    forceScrolledState: boolean;
    public anim = 'start';
    environment = environment;
    constructor(private universityService: UniversityService, private router: Router) {}

    ngOnInit() {
        this.assureAndSubToUniversities();
        const lang = localStorage.getItem('language');
        this.subscriptions.push(this.router.events.subscribe((ev: Event) => {
            if (ev instanceof NavigationEnd) {
                this.forceScrolledState = false;
                if (ev.url.includes('/university/')) {
                    this.forceScrolledState = true;
                }
                if (this.menuMobileExpandedStatus) {
                    this.expandMenu();
                }
            }
        }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
    }

    assureAndSubToUniversities() {
        this.universityService.universities.subscribe(
            (resp: University[]) => this.universities = resp,
        );
    }

    @HostListener('window:scroll', [])
    onWindowsScroll() {
        const menuEl = document.getElementById('menu');
        const hasScrolled = window.pageYOffset > (menuEl ? menuEl.offsetTop : 0) + 10;
        this.isScrolled = hasScrolled;
    }

    closeHambugerAnimation(callback?: Function): void {
        this.menuMobileClosingUp = true;
        setTimeout(() => {
            this.menuMobileClosingUp = false;
            if (callback) {
                callback();
            }
        }, 500);
    }

    closeMenu(): void {
        this.closeHambugerAnimation(() => {
            this.menuMobileExpandedStatus = false;
        });
    }
    expandMenu(): void {
        if (this.menuMobileExpandedStatus) {
            this.closeHambugerAnimation(() => {
                this.menuMobileExpandedStatus = false;
            });
        } else {
            this.menuMobileExpandedStatus = true;
        }
    }
}
