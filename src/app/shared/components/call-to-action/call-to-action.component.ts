import { Component, ViewEncapsulation, Input } from '@angular/core';

@Component({
    selector: 'call-to-action',
    templateUrl: './call-to-action.component.html',
    styleUrls: ['./call-to-action.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CallToActionComponent {
    @Input() settings;
    constructor() {}
}
