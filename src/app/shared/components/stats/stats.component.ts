import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-stats',
    templateUrl: './stats.component.html',
    styleUrls: ['./stats.component.scss'],
})
export class StatsComponent implements OnInit {
    @Input() stats: any;
    @Input() statCounts: any;
    @Input() isLanding: boolean;
    constructor() {}

    ngOnInit(): void {}
}
