import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'stores',
    templateUrl: './stores.component.html',
    styleUrls: ['./stores.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class StoresComponent{

    constructor() { }
}
