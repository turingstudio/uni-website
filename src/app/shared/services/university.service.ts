import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ApiService, ApiResponse } from './api.service';
import { University } from '../abstraction/university';

@Injectable({
    providedIn: 'root',
})
export class UniversityService extends ApiService {
    universities: BehaviorSubject<University[]>;
    private isLoading: boolean = false;
    constructor(private httpClient: HttpClient) {
        super();
        this.universities = new BehaviorSubject<University[]>(null);
    }

    assure(refresh?: boolean): void {
        if (!this.isLoading && (refresh || !this.universities.value)) {
            this.read().subscribe();
        }
    }

    private read(): Observable<University[]> {
        return this.httpClient.get<ApiResponse>(this.getUrl('page', 'universities')).pipe(
            tap(response => this.universities.next(response.data.universities)),
            map(response => response.data),
        );
    }

    readBySlug(slug: string): Observable<any> {
        return this.httpClient.get<ApiResponse>(this.getUrl('page', 'university', slug)).pipe(
            map(response => response.data),
        );
    }
}
