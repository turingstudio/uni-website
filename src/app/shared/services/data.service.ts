import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ApiService, ApiResponse } from './api.service';
import { Terms } from '../abstraction/terms';

@Injectable({
    providedIn: 'root',
})
export class DataService extends ApiService {
    constructor(private httpClient: HttpClient) {
        super();
    }

    getStats(): Observable<any> {
        return this.httpClient.get<ApiResponse>(this.getUrl('page', 'stats')).pipe(map(response => response.data));
    }

    getTerms(): Observable<Terms> {
        return this.httpClient.get<ApiResponse>(this.getUrl('data', 'terms')).pipe(map(response => response.data));
    }

    getPrivacyData(): Observable<Terms> {
        return this.httpClient.get<ApiResponse>(this.getUrl('data', 'policy')).pipe(map(response => response.data));
    }
}
