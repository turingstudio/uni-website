import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { ApiService, ApiResponse } from './api.service';
import { Terms } from '../abstraction/terms';

@Injectable({
    providedIn: 'root',
})
export class ContactService extends ApiService {
    constructor(private httpClient: HttpClient) {
        super();
    }

    create(contactForm): Observable<Terms> {
        return this.httpClient.post<ApiResponse>(this.getUrl('page', 'contact'), contactForm).pipe(map(response => response.data));
    }
}
