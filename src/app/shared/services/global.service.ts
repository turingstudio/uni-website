import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Injectable({
    providedIn: 'root',
})
export class GlobalService {
    constructor() {}
    markFormAsDirty(form: FormGroup): void {
        Object.keys(form.controls).forEach(key => {
            form.get(key).markAsDirty();
            form.get(key).updateValueAndValidity();
        });
    }
}
