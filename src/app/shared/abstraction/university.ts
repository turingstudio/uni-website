export interface University {
    name: string;
    slug: string;
}
