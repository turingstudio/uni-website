import { Component, ViewEncapsulation, Input } from '@angular/core';

@Component({
    selector: 'app-uni-header',
    templateUrl: './uni-header.component.html',
    styleUrls: ['./uni-header.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class UniHeaderComponent {
    @Input() settings;
    constructor() { }
}
