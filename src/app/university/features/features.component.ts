import { Component, ViewEncapsulation, Input } from '@angular/core';

@Component({
    selector: 'university-features',
    templateUrl: './features.component.html',
    styleUrls: ['./features.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class FeaturesComponent {
    constructor() {}
    @Input() settings;
}
