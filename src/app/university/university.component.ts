import { Component, OnInit } from '@angular/core';
import { UniversityService } from '../shared/services/university.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-university',
    templateUrl: './university.component.html',
    styleUrls: ['./university.component.scss'],
})
export class UniversityComponent implements OnInit {
    private subscriptions: Subscription[] = [];
    public uniSettings: any;
    public uniStats;
    constructor(private universityService: UniversityService, private route: ActivatedRoute) { }

    ngOnInit(): void {
        this.subscriptions.push(this.route.paramMap.subscribe(params => {
            const slug = params.get('slug');
            this.universityService.readBySlug(slug).subscribe(
                resp => {
                    this.uniSettings = resp.university;
                    this.uniStats = resp.stats;
                    const uniHeader = resp.university.header.split('/upload/');
                    this.uniSettings.header = `${uniHeader[0]}/upload/w_300,h_300,c_lfill/${uniHeader[1]}`;
                },
            );
        }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
    }

}
