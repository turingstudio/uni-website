import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TermsComponent } from './terms/terms/terms.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LandingPageComponent } from './landing/landing-page.component';
import { UniversityComponent } from './university/university.component';

const routes: Routes = [
    { path: '', component: LandingPageComponent },
    { path: 'terms-and-conditions', component: TermsComponent },
    { path: 'privacy-policy', component: TermsComponent },
    { path: 'about-us', component: AboutUsComponent },
    { path: 'contact-us', component: ContactUsComponent },
    { path: 'university/:slug', component: UniversityComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { scrollOffset: [0, 0], scrollPositionRestoration: 'enabled' })],
    exports: [RouterModule],
})
export class AppRoutingModule {}
