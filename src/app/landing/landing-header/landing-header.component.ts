import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-landing-header',
    templateUrl: './landing-header.component.html',
    styleUrls: ['./landing-header.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LandingHeaderComponent {
    constructor() {}
}
