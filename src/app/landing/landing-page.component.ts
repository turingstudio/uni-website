import { Component, OnInit } from '@angular/core';
import { DataService } from '../shared/services/data.service';

@Component({
    selector: 'app-landing-page',
    templateUrl: './landing-page.component.html',
    styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
    public landingStats;
    constructor(private dataService: DataService) { }

    ngOnInit(): void {
        this.dataService.getStats().subscribe(
            response => this.landingStats = response,
        );
    }

}
